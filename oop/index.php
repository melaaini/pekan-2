<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("Shaun");
echo "Animal's name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold blooded type : ". $sheep->cold_blooded . "<br> <br>"; // "no"

$kodok = new frog("Buduk");
echo "Animal's name : " . $kodok->name . "<br>"; 
echo "Legs : " . $kodok->legs . "<br>"; 
echo "Cold blooded type : ". $kodok->cold_blooded . "<br>";
echo $kodok -> jump("Hop Hop") . "<br>";

$sungokong = new ape("Kera sakti");
echo "Animal's name : " . $sungokong->name . "<br>"; 
echo "Legs : " . $sungokong->legs . "<br>"; 
echo "Cold blooded type : ". $sungokong->cold_blooded . "<br>";
echo $sungokong -> yell("Auooo");
?>